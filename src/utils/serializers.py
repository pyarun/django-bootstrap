from rest_auth.serializers import PasswordResetSerializer as DefaultPasswordResetSerializer
from django.conf import settings
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers


class PasswordResetSerializer(DefaultPasswordResetSerializer):
    def get_email_options(self):
        opts = super(PasswordResetSerializer, self).get_email_options()
        opts['extra_email_context'] = {
            'PASSWORD_RESET_URL': settings.PASSWORD_RESET_CONFIRM_URL
        }
        return opts


class AppRegisterSerializer(RegisterSerializer):
    first_name = serializers.CharField(max_length=30, required=True)
    last_name = serializers.CharField(max_length=30, required=False)

    class Meta:
        fields = ('first_name', 'last_name', 'email', 'password')
        exclude = ('username', )
