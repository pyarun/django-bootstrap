from rest_framework.routers import DefaultRouter

from main.views import UserViewSet

router = DefaultRouter(trailing_slash=False)

router.register('users', UserViewSet, base_name="users")

app_name="project"

urlpatterns = router.urls