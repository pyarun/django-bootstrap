

try:
    from .local_settings import *
except:
    from django.core.exceptions import ImproperlyConfigured
    raise ImproperlyConfigured('local_settings.py is not present')
