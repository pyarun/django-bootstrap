import random
import string

def generate_random_username(length=10, prefix=""):
    """
    To generate random username with specific prefix
    """
    from main.models import User

    def new_username():
        username = "".join(random.sample(string.ascii_letters, length))
        return prefix + "-" + username if prefix else username

    username = new_username()

    while True:
        if not User.objects.filter(username=username).exists():
            break
        username = new_username()
    return username
