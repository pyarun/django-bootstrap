from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from main.models import User
from main.serializers import UserDetailSerializer


class UserViewSet(ModelViewSet):
    serializer_class = UserDetailSerializer

    class Meta:
        model=User

    def get_queryset(self):
        return User.objects.all()
    
    def get_object(self):
        if self.action=='me':
            return self.request.user
        else:
            return super(UserViewSet, self).get_object()

    @action(methods=['get', 'post'], detail=False)
    def me(self, request):
        return self.retrieve(request)