from rest_framework import serializers

from main.models import User


class UserListSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')
    class Meta:
        model=User
        fields = ('id', 'email', 'is_active', 'full_name', 'last_login')


class UserDetailSerializer(UserListSerializer):
    class Meta(UserListSerializer.Meta):
        model=User
        fields = ('id', 'email', 'is_active', 'full_name', 'phone', 'is_superuser', 'is_staff', 'last_login')