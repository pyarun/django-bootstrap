from project.utils import generate_random_username


def set_username(sender, **kwargs):
    """
    Set Username for new Users
    """
    is_new_instance = not bool(kwargs["instance"].pk)
    if is_new_instance:
        if not kwargs["instance"].username:
            kwargs["instance"].username = generate_random_username()
