from django.contrib.auth.models import AbstractUser
from django.contrib.sites.models import pre_save
from django.db import models
from django.db.models.signals import post_save, post_delete

from .signals import set_username
from django_mongoengine import document, fields
from mongoengine import fields

def setup_profile(sender, *args, **kwargs):
    if kwargs['created']:
        profile = UserProfile.objects.create(user_id=kwargs['instance'].id)


def delete_profile(sender, *args, **kwargs):
    instance = kwargs['instance']
    instance.profile.delete()


class User(AbstractUser):
    """
    User profile table with common user fields.
    """
    _profile=None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    phone = models.CharField(max_length=10)

    def get_full_name(self):
        name = super(User, self).get_full_name()
        return name or self.email or self.username

    @property
    def profile(self):
        if not self._profile:
            self._profile = UserProfile.objects.get(user_id=self.id)
        return self._profile


pre_save.connect(set_username, sender=User)
post_save.connect(setup_profile, sender=User)
post_delete.connect(delete_profile, sender=User)


def set_email_as_unique():
    """
    Sets the email field as unique=True in auth.User Model
    """
    fields = dict([(field.name, field) for field in User._meta.fields])
    email_field = fields["email"]
    setattr(email_field, '_unique', True)
    setattr(email_field, 'db_index', True)
    setattr(email_field, 'null', False)
    setattr(email_field, 'blank', False)


set_email_as_unique()


class EmbedAddress(document.EmbeddedDocument):
    line1=fields.StringField(max_length=100)
    line2 = fields.StringField(max_length=100)
    city = fields.StringField(max_length=100)
    state = fields.StringField(max_length=100)
    country = fields.StringField(max_length=100)
    pincode = fields.StringField(max_length=6)


class EmbedIdentity(document.EmbeddedDocument):
    IDENTITY_TYPE_CHOICES = (
        ('aadhar', 'Aadhar'),
        ('dl', 'Driving License'),
    )
    itype=fields.StringField(choices=IDENTITY_TYPE_CHOICES)
    num = fields.StringField(max_length=32)


class UserProfile(document.Document):
    GENDER_CHOICES=(
        ('m', 'Male'),
        ('f', 'Female'),
        ('t', 'Trans')
    )
    user_id = fields.IntField(unique=True)
    gender = fields.StringField(choices=GENDER_CHOICES)
    is_driver = fields.BooleanField(default=False)
    address = fields.EmbeddedDocumentField(EmbedAddress)
    identities = fields.EmbeddedDocumentListField(EmbedIdentity)


